function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units in area.",
		parameterDefs = {
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.radius then
		return FAILURE
	end

	local team =  Spring.GetLocalTeamID()

	local transportsFull = 0
	local workingTransports = 0

	for i = 1, #units, 1 do
		local x,y,z = Spring.GetUnitPosition(units[i])
		local pos = Vec3(x,y,z)

		local frendlies = Spring.GetUnitsInCylinder(pos.x, pos.z, parameter.radius, team)
		local defID = Spring.GetUnitDefID(units[i])
		local transporting = #Spring.GetUnitIsTransporting(units[i])
		local capacity = UnitDefs[defID].transportCapacity

		if #frendlies == 1 + transporting or transporting == capacity then
			transportsFull = transportsFull + 1
		else
			workingTransports = workingTransports + 1
			if Spring.GetUnitCommands(units[i], 0) == 0 then
				workingTransports = workingTransports - 1
				Spring.GiveOrderToUnit(units[i], CMD.LOAD_UNITS, {pos.x, pos.y, pos.z, parameter.radius} , {})
			end
		end
	end

	if transportsFull ~= 0 and workingTransports == 0 then
		return SUCCESS
	end

	return RUNNING
end