--Ondrelord

function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units to transporters.",
		parameterDefs = {
			{ 
				name = "farks",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "farksIDs",
			},
			{ 
				name = "area",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "area",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.farks or not parameter.area then
		return FAILURE
	end

	local center = parameter.area.center
	local radius = parameter.area.radius
	local farks = parameter.farks

	for i = 1, #farks, 1 do
		if Spring.ValidUnitID(farks[i]) then 
			if #Spring.GetUnitCommands(farks[i], 3) == 0  then
				Spring.GiveOrderToUnit(farks[i], CMD.RECLAIM, {center.x, center.y, center.z, radius}, {})
			end
		end
	end

	return RUNNING
end