function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Uses list of positions in reverse order as path." ,
		parameterDefs = {
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{Vec3(0,0,0)}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.path then
		return FAILURE
	end

	if #parameter.path < #units then
		return FAILURE
	end

	local inPosition = 0

	for u = 1, #units, 1 do
		local x, y, z = Spring.GetUnitPosition(units[u])
		local pathEnd = parameter.path[u][1]
		local distance = math.sqrt((x - pathEnd.x) * (x - pathEnd.x) + (z - pathEnd.z) * (z - pathEnd.z))

		if distance < 150 then
			inPosition = inPosition + 1
		else
			if Spring.GetUnitCommands(units[u], 0) == 0 then
				for i = #parameter.path[u], 1, -1 do 
					Spring.GiveOrderToUnit(units[u], CMD.MOVE, parameter.path[u][i]:AsSpringVector(), {"shift"})
				end
			end
		end
	end

	if inPosition == #units then
		return SUCCESS
	end

	return RUNNING
end