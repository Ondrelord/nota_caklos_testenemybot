--Ondrelord

function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Unload units in area.",
		parameterDefs = {
			{ 
				name = "transp",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "transporterIDs",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},
			{
				name = "regroupPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end

function copyList(list)
	local newList = {}

	for k,v in pairs(list) do
		newList[k] = v
	end
	return newList
end

function distance(pos, from)
	return math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
end

function Run(self, unitIDs, parameter)
	if not parameter.pos or not parameter.radius or not parameter.transp then
		return FAILURE
	end

	local regroupPos = {}
	if not parameter.regroupPos then
		regroupPos = copyList(parameter.pos)
		regroupPos.x = regroupPos.x + parameter.radius
		regroupPos.z = regroupPos.z + parameter.radius
	else
		regroupPos = parameter.regroupPos
	end

	local units = parameter.transp

	local nonEmpty = 0

	for i = 1, #units, 1 do
		if Spring.ValidUnitID(units[i]) then
			if #Spring.GetUnitIsTransporting(units[i]) > 0 then
				nonEmpty = nonEmpty + 1
				if Spring.GetUnitCommands(units[i], 0) == 0 then
					Spring.GiveOrderToUnit(units[i], CMD.UNLOAD_UNITS, {parameter.pos.x, parameter.pos.y, parameter.pos.z, parameter.radius}, {})
					
				end
			else
				local x,y,z = Spring.GetUnitPosition(units[i])
				if distance(Vec3(x,y,z), regroupPos) > 50 + (10 * #units) then
					nonEmpty = nonEmpty + 1 
					if Spring.GetUnitCommands(units[i], 0) == 0 then
						Spring.GiveOrderToUnit(units[i], CMD.MOVE, {regroupPos.x, regroupPos.y, regroupPos.z}, {})
					end
				end
			end
		end
	end

	if nonEmpty == 0 then
		return SUCCESS
	end

	return RUNNING
end