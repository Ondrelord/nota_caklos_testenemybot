function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units in area.",
		parameterDefs = {
			{ 
				name = "id",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "1",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.id then
		return FAILURE
	end

	if Spring.GetUnitIsTransporting(units[1]) == 1 then
		return SUCCESS
	else
		Spring.GiveOrder(CMD.LOAD_ONTO, parameter.id , {})
		return RUNNING
	end
end