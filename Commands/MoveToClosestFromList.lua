function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. " ,
		parameterDefs = {
			{ 
				name = "targets",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{Vec3(0,0,0)}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.targets then
		return FAILURE
	end

	local succ = 0
	for i = 1, #units, 1 do
		local x,y,z = Spring.GetUnitPosition(units[i])
		local closest = math.huge
		local target

		for j = 1, #parameter.targets, 1 do
			local dist = math.sqrt((parameter.targets[j].x - x) * (parameter.targets[j].x - x) + (parameter.targets[j].z - z) * (parameter.targets[j].z - z))
			if closest > dist then
				closest = dist
				target = parameter.targets[j]
			end
		end

		Spring.GiveOrderToUnit(units[i], CMD.MOVE, target:AsSpringVector(), {})
		--succ = succ + 1
	end

	if succ == #units then
		return SUCCESS
	end
	return RUNNING
end