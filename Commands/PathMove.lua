function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Uses list of positions as path." ,
		parameterDefs = {
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{Vec3(0,0,0)}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.path then
		return FAILURE
	end

	local x, y, z = Spring.GetUnitPosition(units[1])
	local pathEnd = parameter.path[#parameter.path]
	local distance = math.sqrt((x - pathEnd.x) * (x - pathEnd.x) + (z - pathEnd.z) * (z - pathEnd.z))
	if distance < 50 then
		return SUCCESS
	end

	if Spring.GetUnitCommands(units[1], 0) > 0 then
		return RUNNING
	end

	for i = 1, #parameter.path, 1 do 
		Spring.GiveOrderToUnit(units[1], CMD.MOVE, parameter.path[i]:AsSpringVector(), {"shift"})
	end

	return RUNNING
end