function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "leader",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.leader then
		return FAILURE
	end

	local leadX,leadY,leadZ = Spring.GetUnitPosition(parameter.leader)
	local leaderPos = Vec3(leadX,leadY,leadZ)


	for u = 1, #units, 1 do
		local unitX,unitY,unitZ = Spring.GetUnitPosition(units[u])

		local distance = math.sqrt((unitX - leadX) * (unitX - leadX) + (unitZ - leadZ) * (unitZ - leadZ))

		if distance > 200 then
			if Spring.GetUnitCommands(units[1], 0) < 5 then 
				Spring.GiveOrderToUnit(units[u], CMD.MOVE, leaderPos:AsSpringVector(), {"shift"})
				
			end
		end
	end

	return RUNNING
end