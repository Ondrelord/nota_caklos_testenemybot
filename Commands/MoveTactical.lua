function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end

function distance(pos, from)
	return math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
end

function Run(self, unitIDs, parameter)
	if not parameter.pos or not parameter.group then
		return FAILURE
	end

	local units = parameter.group
	local leader = units[1]


	if Spring.GetUnitCommands(units[1], 0) == 0 then 
		Spring.GiveOrderToUnit(units[1], CMD.MOVE, parameter.pos:AsSpringVector(), {} )
	end

	local leadx,leady,leadz = Spring.GetUnitPosition(leader)
	local leaderPos = Vec3(leadx,leady,leadz)

	local cdist = distance(leaderPos, parameter.pos)
	local adist = leaderPos.x - parameter.pos.x
	local Direction = math.asin(adist/cdist) * adist/math.abs(adist)

	local separation = 25

	for i = 2, #units, 1 do
		local x = parameter.pos.x + math.cos(Direction) * separation * (i-1)
		local z = parameter.pos.z + math.sin(Direction) * separation * (i-1)
		local y = Spring.GetGroundHeight(x,z)

		local targetPos = Vec3(x,y,z)

		if Spring.GetUnitCommands(units[i], 0) == 0 then 
			Spring.GiveOrderToUnit(units[i], CMD.MOVE, targetPos:AsSpringVector(), {} )
		end
	end


	local distGoal = distance(leaderPos, parameter.pos)

	if distGoal < 50 then
		return SUCCESS
	else
		return RUNNING
	end
end