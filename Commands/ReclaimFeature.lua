--Ondrelord

function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units to transporters.",
		parameterDefs = {
			{ 
				name = "farks",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "farksIDs",
			},
			{ 
				name = "features",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "featuresIDs",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.farks or not parameter.features then
		return FAILURE
	end

	local features = parameter.features
	local farks = parameter.farks

	local stillRunning = false
	for x = 1, #features, 1 do
		if Spring.ValidFeatureID(features[x]) then
			stillRunning = true
			break
		end
	end
	if not stillRunning then
		return SUCCESS
	end

	local featIDX = 1

	for i = 1, #farks, 1 do
		if Spring.ValidUnitID(farks[i]) then 
			if #Spring.GetUnitCommands(farks[i]) == 0  then
				while not Spring.ValidFeatureID(features[featIDX]) do
					featIDX = featIDX + 1
					if featIDX > #features then
						break
					end
				end

				Spring.GiveOrderToUnit(farks[i], CMD.RECLAIM, {Game.maxUnits+features[featIDX]}, {})

				featIDX = featIDX + 1
				if featIDX > #features then
					featIDX = 1
				end
			end
		end
	end

	return RUNNING
end