function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, unitIDs, parameter)
	if not parameter.pos then
		return FAILURE
	end

	if not parameter.pos.y then
		parameter.pos.y = Spring.GetGroundHeight(parameter.pos.x, parameter.pos.z)
	end

	local x,y,z = Spring.GetUnitPosition(unitIDs[1])

	if Sensors.nota_caklos_testenemybot.DistanceBetween(parameter.pos, Vec3(x,y,z)) < 50 then
		return SUCCESS
	end

	local position = Vec3(parameter.pos.x, parameter.pos.y, parameter.pos.z)

	if Spring.GetUnitCommands(unitIDs[1], 0) < 5 then 
		Spring.GiveOrderToUnit(unitIDs[1], CMD.MOVE, position:AsSpringVector(), {"shift"} )
		return RUNNING
	end

	return SUCCESS		
end