function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "farks",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			},
			{ 
				name = "area",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.farks or not parameter.area then
		return FAILURE
	end

	local center = parameter.area.center
	local radius = parameter.area.radius

	local HPState = Sensors.nota_caklos_testenemybot.GetHPStateInArea(parameter.area)

	local farks = parameter.farks

	for i = 1, #farks, 1 do
		if Spring.GetUnitCommands(farks[i], 0) == 0 then 
			Spring.GiveOrderToUnit(farks[i], CMD.REPAIR, {center.x, center.y, center.z, radius}, {})
		end
	end

	if #HPState.critical == 0 and #HPState.damaged == 0 then
		return SUCCESS
	else
		return RUNNING
	end
end