function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{Vec3(0,0,0)}",
			},
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIds",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end


function Run(self, unitIDs, parameter)
	if not parameter.path and not parameter.group then
		return FAILURE
	end

	if #parameter.path == 0 then
		return SUCCESS
	end

	local fight = parameter.fight
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pathEnd = parameter.path[#parameter.path]

	local stillRunning = false

	for groupIDX = 1, #parameter.group, 1 do
		if Spring.ValidUnitID(parameter.group[groupIDX]) then
			if #Spring.GetUnitCommands(parameter.group[groupIDX], 3) == 0 then
				for pathIDX = 1, #parameter.path, 1 do 
					Spring.GiveOrderToUnit(parameter.group[groupIDX], cmdID, parameter.path[pathIDX]:AsSpringVector(), {"shift"})
				end
			end
			
			local x, y, z = Spring.GetUnitPosition(parameter.group[groupIDX])
			
			local distance = math.sqrt((x - pathEnd.x) * (x - pathEnd.x) + (z - pathEnd.z) * (z - pathEnd.z))
			if distance > 50 then
				stillRunning = true
			end
		end
	end

	if not stillRunning then 
		return SUCCESS
	end

	return RUNNING

end