function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, unitIDs, parameter)
	if not parameter.pos or not parameter.group then
		return FAILURE
	end

	local units = parameter.group

	local distance = 0

	for i = 1, #units, 1 do
		if Spring.ValidUnitID(units[i]) then
			local x,y,z = Spring.GetUnitPosition(units[i])
			distance = distance + math.sqrt((x - parameter.pos.x) * (x - parameter.pos.x) + (z - parameter.pos.z) * (z - parameter.pos.z))
		end
	end
	distance = distance / #units

	if distance < 50+(#units*2) then
		return SUCCESS
	else
		for i = 1, #units, 1 do
			if Spring.ValidUnitID(units[i]) then
				if Spring.GetUnitCommands(units[i], 0) == 0 then 
					Spring.GiveOrderToUnit(units[i], CMD.MOVE, parameter.pos:AsSpringVector(), {} )
				end
			end
		end
		return RUNNING
	end
end