function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Unloads one unit.",
		parameterDefs = {
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.pos then
		return FAILURE
	end

	local position = parameter.pos + Vec3(10,0,10)
	for i = 1, #units, 1 do
		Spring.GiveOrderToUnit(units[1], CMD.UNLOAD_UNIT, position:AsSpringVector() , {})
	end
	return SUCCESS
end