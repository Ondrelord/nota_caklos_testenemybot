function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end

function distance(pos, from)
	return math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
end


function Run(self, unitIDs, parameter)
	if not parameter.pos or not parameter.group then
		return FAILURE
	end

	local units = parameter.group

	for i = 1, #units, 1 do
		if Spring.ValidUnitID(units[i]) then
			local myx,myy,myz = Spring.GetUnitPosition(units[i])
			local myPos = Vec3(myx,myy,myz)
			local enemy = Spring.GetUnitNearestEnemy(units[i])
			if enemy == nil then
				break
			end
			local enx,eny,enz = Spring.GetUnitPosition(enemy)
			local enemyPos = Vec3(enx,eny,enz) 

			if distance(myPos, enemyPos) < 400 then
				Spring.GiveOrderToUnit(units[i], CMD.MOVE, parameter.pos:AsSpringVector(), {})
			end
		end
	end

	return RUNNING
end