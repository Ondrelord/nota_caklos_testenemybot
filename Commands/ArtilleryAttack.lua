function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end

function distance(pos, from)
	return math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
end


function Run(self, unitIDs, parameter)
	if not parameter.pos or not parameter.group then
		return FAILURE
	end

	local units = parameter.group

	for i = 1, #units, 1 do
		if Spring.ValidUnitID(units[i]) then
			local unitDef = Spring.GetUnitDefID(units[i])
			local weaponDef = UnitDefs[unitDef].weapons[1].weaponDef
			local weaponRange = WeaponDefs[weaponDef].range

			local x,y,z = Spring.GetUnitPosition(units[i])
			local myPos = Vec3(x,y,z)

			if distance(parameter.pos, myPos) > weaponRange then
				--Spring.GiveOrderToUnit(units[i], CMD.FIRE_STATE, CMD.FIRESTATE_FIREATWILL, {})
				Spring.GiveOrderToUnit(units[i], CMD.MOVE, parameter.pos:AsSpringVector(), {})
			else
				--Spring.GiveOrderToUnit(units[i], CMD.FIRE_STATE, 2, {})
				Spring.GiveOrderToUnit(units[i], CMD.ATTACK, parameter.pos:AsSpringVector(), {})
			end
		end
	end

	return RUNNING
end