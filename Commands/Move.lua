--Ondrelord

function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.pos then
		return FAILURE
	end

	local distance = 0

	for i = 1, #units, 1 do
		local x,y,z = Spring.GetUnitPosition(units[i])
		distance = distance + math.sqrt((x - parameter.pos.x) * (x - parameter.pos.x) + (z - parameter.pos.z) * (z - parameter.pos.z))
	end
	distance = distance / #units

	if distance < 50+(#units*10) then
		return SUCCESS
	else
		if Spring.GetUnitCommands(units[1], 0) == 0 then 
			Spring.GiveOrder( CMD.MOVE, parameter.pos:AsSpringVector(), {} )
		end
		return RUNNING
	end
end