function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Uses list of positions as path." ,
		parameterDefs = {
			{ 
				name = "unitCommands",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{Vec3(0,0,0)}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.unitCommands then
		return FAILURE
	end

	local inPosition = 0
	local movingUnits = 0

	for Unit,Path in pairs(parameter.unitCommands) do
		if Spring.ValidUnitID(Unit) and not Spring.GetUnitIsDead(Unit) and #Path ~= 0 then
			local x, y, z = Spring.GetUnitPosition(Unit)
			local pathEnd = Path[1]
			local distance = math.sqrt((x - pathEnd.x) * (x - pathEnd.x) + (z - pathEnd.z) * (z - pathEnd.z))

			movingUnits = movingUnits + 1 

			if distance < 150 then
				inPosition = inPosition + 1
			else
				if Spring.GetUnitCommands(Unit, 0) == 0 then
					for i = #Path, 1, -1 do 
						Spring.GiveOrderToUnit(Unit, CMD.MOVE, Path[i]:AsSpringVector(), {"shift"})
					end
				end
			end
		else
			parameter.unitCommands[Unit] = nil
		end
	end

	if inPosition == movingUnits then
		return SUCCESS
	end

	return RUNNING
end