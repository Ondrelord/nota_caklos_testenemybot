function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units in area.",
		parameterDefs = {
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.pos or not parameter.radius then
		return FAILURE
	end

	if Spring.GetUnitCommands(units[1], 0) > 0 then
		return RUNNING
	end

	local frendlies = Sensors.nota_caklos_testenemybot.GetFriendlyUnits(Sensors.core.Position(), parameter.radius)
	local defID = Spring.GetUnitDefID(units[1])
	local transporting = #Spring.GetUnitIsTransporting(units[1])
	local capacity = UnitDefs[defID].transportCapacity

	if #frendlies == 1 + transporting or transporting == capacity then
		return SUCCESS
	else
		
		Spring.GiveOrderToUnit(units[1], CMD.LOAD_UNITS, {parameter.pos.x, parameter.pos.y, parameter.pos.z, parameter.radius} , {})
		return RUNNING
	end
end