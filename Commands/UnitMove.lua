function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. ",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitID",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.pos or not parameter.unit then
		return FAILURE
	end

	if Spring.GetUnitCommands(parameter.unit, 0) > 0 then
		return RUNNING
	end

	local x,y,z = Spring.GetUnitPosition(parameter.unit)
	local distance = math.sqrt((x - parameter.pos.x) * (x - parameter.pos.x) + (z - parameter.pos.z) * (z - parameter.pos.z))
	if distance < 50 then
		return SUCCESS
	end

	Spring.GiveOrderToUnit(parameter.unit, CMD.MOVE, parameter.pos:AsSpringVector(), {})
	return RUNNING
end