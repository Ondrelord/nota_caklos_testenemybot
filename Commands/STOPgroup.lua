function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "MOST IMPORTANT COMMAND IN NOTA AI DEVELOPMENT!!!",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.group then
		return FAILURE
	end

	for i = 1, #parameter.group, 1 do
		Spring.GiveOrderToUnit(parameter.group[i], CMD.STOP, {}, {} )
	end
	return SUCCESS
end