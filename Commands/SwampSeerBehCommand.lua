function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Simple Move to target position by all units.",
		parameterDefs = {
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitIDs",
			},
			{ 
				name = "frontline",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "frontline",
			},
			{ 
				name = "basePos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "frontline",
			},
		}
	}
end


function Run(self, unitIDs, parameter)
	if not parameter.frontline or not parameter.basePos or not parameter.group then
		return FAILURE
	end

	local units = parameter.group

	local distance = 0

	local stillRunning = false

	for i = 1, #units, 1 do
		if Spring.ValidUnitID(units[i]) then
			local x,y,z = Spring.GetUnitPosition(units[i])

			local pos = {}
			local closest = math.huge
			for _,v in pairs(parameter.frontline) do
				local dist = math.sqrt((x - v.x) * (x - v.x) + (z - v.z) * (z - v.z))
				if closest > dist then
					closest = dist
					pos = v
				end
			end

			if closest > 2000 then 
				pos = parameter.basePos
				closest = math.sqrt((x - pos.x) * (x - pos.x) + (z - pos.z) * (z - pos.z))
			end

			if closest > 100 then
				if Spring.GetUnitCommands(units[i], 0) == 0 then 
					Spring.GiveOrderToUnit(units[i], CMD.MOVE, pos:AsSpringVector(), {} )
					stillRunning = true
				end
			end
		end
	end

	if stillRunning then
		return RUNNING
	else
		return SUCCESS
	end
end