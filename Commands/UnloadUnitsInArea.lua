function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Unload units in area.",
		parameterDefs = {
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},
			{
				name = "regroupPos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.pos or not parameter.radius then
		return FAILURE
	end

	local nonEmpty = 0

	for i = 1, #units, 1 do
		if #Spring.GetUnitIsTransporting(units[i]) > 0 then
			nonEmpty = nonEmpty + 1
			if Spring.GetUnitCommands(units[i], 0) == 0 then
				Spring.GiveOrderToUnit(units[i], CMD.UNLOAD_UNITS, {parameter.pos.x, parameter.pos.y, parameter.pos.z, parameter.radius}, {})
				if parameter.regroupPos ~= nil then
					Spring.GiveOrderToUnit(units[i], CMD.MOVE, {parameter.regroupPos.x, parameter.regroupPos.y, parameter.regroupPos.z}, {"shift"})
				end
			end
		end
		
	end

	if nonEmpty == 0 then
		return SUCCESS
	end

	return RUNNING
end