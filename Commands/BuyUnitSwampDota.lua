-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Triggers manual mission end. Only missions which are designed to listed to this type of node are ended.",
		parameterDefs = {
			{ 
				name = "unitName",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitName",
			},
			{ 
				name = "count",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "1",
			},
			{ 
				name = "prices",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{}",
			},
	}
	}
end

function Run(self, units, parameter)
	if not parameter.unitName or not parameter.count or not parameter.prices then 
		return FAILURE
	end

	local price = parameter.prices
	local team =  Spring.GetLocalTeamID()
	local metal,a,s,d,f,g,h,j,k = Spring.GetTeamResources(team, "metal")

	if price[parameter.unitName]*parameter.count >= metal then
		return FAILURE
	end

	for i = 1, parameter.count, 1 do
		
		    message.SendRules({
		        subject = "swampdota_buyUnit",
		        data = { unitName = parameter.unitName},
		    })
	end
	
	return SUCCESS
end


function Reset(self)
	return self
end