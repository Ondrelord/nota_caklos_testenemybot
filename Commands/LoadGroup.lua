--Ondrelord

function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Load units to transporters.",
		parameterDefs = {
			{ 
				name = "transp",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "transporterIDs",
			},
			{ 
				name = "group",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unitsIDs",
			}
		}
	}
end


function Run(self, units, parameter)
	if not parameter.transp or not parameter.group then
		return FAILURE
	end

	local transporters = parameter.transp
	local group = parameter.group

	local allIn = true
	local fullCap = true

	for i = 1, #transporters, 1 do
		if Spring.ValidUnitID(transporters[i]) then
			local defID = Spring.GetUnitDefID(transporters[i])
			if #Spring.GetUnitIsTransporting(transporters[i]) ~= UnitDefs[defID].transportCapacity then
				fullCap = false
				break
			end
		end
	end

	for i = 1, #group, 1 do
		if not Spring.GetUnitTransporter(group[i]) then
			allIn = false
			break
		end
	end

	if allIn or fullCap then		
		return SUCCESS
	else
		local Gi = 1
		for Ti = 1, #transporters, 1 do
			if Spring.ValidUnitID(transporters[Ti]) then
				local defID = Spring.GetUnitDefID(transporters[Ti])
				
				for cap = #Spring.GetUnitIsTransporting(transporters[Ti]), UnitDefs[defID].transportCapacity-1, 1 do
					if #Spring.GetUnitCommands(transporters[Ti]) == 0 then
						Spring.GiveOrderToUnit(group[Gi], CMD.LOAD_ONTO, {transporters[Ti]} , {"right"})
					end
					Gi = Gi + 1

					if Gi > #group then
						break
					end
				end
				
				if Gi > #group then
					break
				end
			end
		end
		return RUNNING
	end
end