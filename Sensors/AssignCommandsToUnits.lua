local sensorInfo = {
	name = "Assign",
	desc = "",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function copyList(list)
	local newList = {}

	for i = 1, #list, 1 do
		newList[i] = list[i]
	end
	return newList
end

-- @description return current wind statistics
return function(unitos, commands)
	for comK,comV in  pairs(commands) do
		for uK,uV in pairs(unitos) do
			if #uV == 0 then
				unitos[uK] = copyList(comV)
				commands[comK] = nil
				break
			end
		end
	end

	return unitos
end