local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(squads)
	local updatedSquads = {}

	for num,squad in pairs(squads) do
		local newSquad = {}
		for _,unit in pairs(squad.units) do
			if Spring.ValidUnitID(unit) then
				newSquad[#newSquad + 1] = unit
			end
		end
		if #newSquad > 0 then
			updatedSquads[num] = {units = newSquad, type = squad.type}
		end
	end

	return updatedSquads
end