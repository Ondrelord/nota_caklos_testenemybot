local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(allFarks, farkSquads)
	local nonassignedFarks = {}

	if farkSquads == nil then
		farkSquads = {Top = {}, Middle = {}, Bottom = {}}
	end

	local topline, midline, botline = {}, {}, {}

	for t = 1, #farkSquads.Top, 1 do
		if Spring.ValidUnitID(farkSquads.Top[t]) then
			topline[#topline + 1] = farkSquads.Top[t]
		end
	end
	for m = 1, #farkSquads.Middle, 1 do
		if Spring.ValidUnitID(farkSquads.Middle[m]) then
			midline[#midline + 1] = farkSquads.Middle[m]
		end
	end
	for b = 1, #farkSquads.Bottom, 1 do
		if Spring.ValidUnitID(farkSquads.Bottom[b]) then
			botline[#botline + 1] = farkSquads.Bottom[b]
		end
	end



	for i = 1, #allFarks, 1 do
		local found = false
		for _,line in pairs(farkSquads) do
			for u = 1, #line, 1 do
				if line[u] == allFarks[i] then
					found = true
					break
				end
			end

			if found then
				break
			end
		end

		if not found then
			nonassignedFarks[#nonassignedFarks + 1] = allFarks[i]
		end
	end


	for n = 1, #nonassignedFarks, 1 do
		if #topline <= #botline and #topline <= #midline then
			topline[#topline + 1] = nonassignedFarks[n]
		elseif #midline <= #botline then
			midline[#midline + 1] = nonassignedFarks[n]
		else
			botline[#botline + 1] = nonassignedFarks[n]
		end
	end

	farkSquads = {
		Top = topline,
		Middle = midline,
		Bottom = botline
	}
	return farkSquads
end