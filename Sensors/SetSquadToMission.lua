local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(squads, missions, missionName, typ)
	for sk,sv in pairs(squads) do
		if sv.type == typ then
			local found = false
			for _,mv in pairs(missions) do
				if sk == mv.squad then
					found = true
				end
				break
			end
			if not found then
				missions[missionName] = {squad = sk}
			end
		end
	end

	return missions
end