local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(corridors)
	local newCorridors = {}

	for k,v in pairs(corridors) do 
		newCorridors[k] = {}
		for i = 1, #v.points, 1 do
			newCorridors[k][i] = v.points[i].position
		end
	end

	return newCorridors
end