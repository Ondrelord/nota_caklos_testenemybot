local sensorInfo = {
	name = "GetIdleUnits",
	desc = "Return list of units that have not been assigned.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(unitos, assigned)
	local unassigned = {}	

	for i = 1, #unitos, 1 do
		local found = false
		
		for a = 1, #assigned, 1 do
			if unitos[i] == assigned[a] then
				found = true
				break
			end
		end

		if not found then
			unassigned[#unassigned + 1] = unitos[i]
		end
	end

	return unassigned
end