local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(corridors)
	local team =  Spring.GetLocalTeamID()
	local frontStrongholds = {}



	for k,v in pairs(corridors) do 
		--frontStrongholds[k] = {}
		for i = 1, #v.points, 1 do
			if v.points[i].isStrongpoint and v.points[i].ownerAllyID == team then
				frontStrongholds[k] = v.points[i].position
			end
		end
	end

	return frontStrongholds
end