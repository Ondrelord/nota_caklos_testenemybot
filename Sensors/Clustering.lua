local sensorInfo = {
	name = "Clustering",
	desc = "Create clusters of positions.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function distance(pos, from)
	return math.sqrt((pos.x - from.x) * (pos.x - from.x) + (pos.z - from.z) * (pos.z - from.z))
end

-- @description 
return function(points, maxDistBetweenPoints)

	--local points = {}
	local clusters = {}
	local clusterPoints = {}
	local clusterSum = {}
	local clusterSize = {}

	clusters[1] = points[1]
	clusterPoints[1] = {}
	clusterPoints[1][1] = points[1]
	clusterSum[1] = Vec3(0,0,0)
	clusterSize[1] = 1

	for pointIDX = 2, #points, 1 do
		local found = false
		for clustIDX = 1, #clusters, 1 do
			for clstPoitIDX = 1, #clusterPoints[clustIDX], 1 do
				local dist = distance(points[pointIDX], clusterPoints[clustIDX][clstPoitIDX])
				if dist < maxDistBetweenPoints then
					clusters[clustIDX] = (clusters[clustIDX]+clusterSum[clustIDX])/clusterSize[clustIDX]
					clusterPoints[clustIDX][#clusterPoints + 1] = points[pointIDX]
					clusterSum[clustIDX] = clusterSum[clustIDX] + points[pointIDX]
					clusterSize[clustIDX] = clusterSize[clustIDX]+1
					found = true
					break
				end
			end

			if found then
				break
			end
		end

		if not found then
			clusters[#clusters+1] = points[pointIDX]
			clusterPoints[#clusterPoints+1] = {}
			clusterPoints[#clusterPoints][1] = points[pointIDX]
			clusterSum[#clusterSum+1] = Vec3(0,0,0)
			clusterSize[#clusterSize+1] = 1
		end
	end

	local clusterInfo = {}

	for c = 1, #clusters, 1 do
		clusterInfo[c] = {center = clusters[c], radius = 0}
		local radius = 0
		for p = 1, #clusterPoints[c], 1 do
			radius = math.max(distance(clusterPoints[c][p], clusters[c]), radius)
		end
		
		clusterInfo[c].radius = radius
	end

	return clusterInfo
	
end