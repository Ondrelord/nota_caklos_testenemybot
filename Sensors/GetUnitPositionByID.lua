local sensorInfo = {
	name = "GetUnitPositionByID",
	desc = "",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function(ID)
	if Spring.ValidUnitID(ID) then
		local x,y,z = SpringGetUnitPosition(ID)
		return Vec3(x,y,z)
	end
	return nil
end