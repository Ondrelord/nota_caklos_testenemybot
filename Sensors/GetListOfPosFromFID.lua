local sensorInfo = {
	name = "GetListOfPosFromFID",
	desc = "Create list of Vec3 positions from featureIDs list.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(featureIDs)

	local positions = {}

	for i = 1, #featureIDs, 1 do
		local x,y,z = Spring.GetFeaturePosition(featureIDs[i])
		positions[#positions + 1] = Vec3(x,y,z)
	end

	return positions
end