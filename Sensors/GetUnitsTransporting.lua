local sensorInfo = {
	name = "GetUnitTransporting",
	desc = "Gets if all transporters are full, how many of them are full and all unitos they are transporting combined.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(unitos)
	local unitsTransported = {}
	local transportersFull = false
	local numFullTransporters = 0

	for i = 1, #unitos, 1 do
		if Spring.ValidUnitID(unitos[i]) then
			local oneTransport = Spring.GetUnitIsTransporting(unitos[i])

			for t = 1, #oneTransport, 1 do
				unitsTransported[#unitsTransported + 1] = oneTransport[t]
			end

			local defID = Spring.GetUnitDefID(unitos[i])
			if UnitDefs[defID].transportCapacity == #oneTransport then
				numFullTransporters = numFullTransporters + 1
			end
		end
	end

	if numFullTransporters == #unitos then
		transportersFull = true
	end

	--return {full = transportersFull, numFull = numFullTransporters, unitsTransported = unitsTransported}
	return unitsTransported
end