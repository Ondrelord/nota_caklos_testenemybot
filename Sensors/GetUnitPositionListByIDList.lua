local sensorInfo = {
	name = "GetUnitPositionLsitByIDList",
	desc = "Gets positions of units from list of IDs to list of Vec3 positions",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

-- @description return static position of the first unit
return function(IDs)
	local positions = {}

	for i = 1, #IDs, 1 do
		local x,y,z = SpringGetUnitPosition(IDs[i])
		positions[i] = Vec3(x,y,z)
	end

	return positions
end