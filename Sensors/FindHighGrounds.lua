local sensorInfo = {
	name = "FindHighGround",
	desc = "",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--speedups
--local DistanceBetween = Sensors.nota_caklos_testenemybot.DistanceBetween


-- @description 
return function(center, radius)
	local currx, curry,currz = Spring.GetUnitPosition(units[1])
	local maxy,maxx,maxz = 0,0,0

	local points = {}
	local clusters = {}
	local clusterInc = {}
	local clusterSize = {}

	for x = 0,  Game.mapSizeX, 32 do
		for z = 0,  Game.mapSizeZ, 32 do
			y = Spring.GetGroundHeight(x, z)
			
			if y > curry +50 then
				points[#points+1] = Vec3(x,y,z)
			end
		end
	end

	clusters[1] = points[1]
	clusterInc[1] = Vec3(0,0,0)
	clusterSize[1] = 1
	for i = 2, #points, 1 do
		local found = false
		for j = 1, #clusters, 1 do
			local dist = math.sqrt((points[i].x - clusters[j].x) * (points[i].x - clusters[j].x) + (points[i].z - clusters[j].z) * (points[i].z - clusters[j].z))
			if dist < 256 then
				clusters[j] = clusters[j]
				clusterInc[j] = clusterInc[j] + points[i]
				clusterSize[j] = clusterSize[j]+1
				found = true
			end
		end
		if not found then
			clusters[#clusters+1] = points[i]
			clusterInc[#clusterInc+1] = Vec3(0,0,0)
			clusterSize[#clusterSize+1] = 1
		end
	end

	for c = 1, #clusters, 1 do
		clusters[c] = (clusters[c]+clusterInc[c])/clusterSize[c]
	end

	return clusters	
	
end