local sensorInfo = {
	name = "GetListOfPosFromUID",
	desc = "Create list of Vec3 positions from unitIDs list.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description 
return function(unitIDs)

	local positions = {}

	for i = 1, #unitIDs, 1 do
		local x,y,z = Spring.GetUnitPosition(unitIDs[i])
		positions[#positions + 1] = Vec3(x,y,z)
	end

	return positions
end