local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

return function(from, to)
	if from == nil or to == nil then
		return -1
	end

	if from.x == nil or from.z == nil or to.x == nil or to.z == nil then
		return -1
	end

	return math.sqrt((from.x - to.x) * (from.x - to.x) + (from.z - to.z) * (from.z - to.z))
end