local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(area)
	local healthy = {}
	local damaged = {}
	local critical = {}

	local unitos = Sensors.nota_caklos_testenemybot.GetFriendlyUnits(area.center, area.radius)

	for i = 1, #unitos, 1 do
		local health = Spring.GetUnitHealth(unitos[i])
		local defID = Spring.GetUnitDefID(unitos[i])

		if health > 0.70*UnitDefs[defID].health then
			healthy[#healthy + 1] = unitos[i]
		elseif health > 0.30*UnitDefs[defID].health then
			damaged[#damaged + 1] = unitos[i]
		else
			critical[#critical + 1] = unitos[i]
		end
	end

	return {healthy = healthy, damaged = damaged, critical = critical}
end