local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = 60

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(assigned, toRescue, rescued, atlases)
	if assigned == nil then
		assigned = {}
	end

	local toRescueIDX = 1 

	for a = 1, #atlases, 1 do
		if toRescueIDX > #toRescue then
			assigned[atlases[a]] = nil
		else
			while not Spring.ValidUnitID(toRescue[toRescueIDX]) do
				toRescueIDX = toRescueIDX + 1
			end

			local found = true

			while found do
				found = false
				for _,v in pairs(assigned) do
					if v == toRescue[toRescueIDX] then
						found = true
						toRescueIDX = toRescueIDX + 1 
						break
					end
				end

				for _,v in pairs(rescued) do
					if v == toRescue[toRescueIDX] then
						found = true
						toRescueIDX = toRescueIDX + 1 
						break
					end
				end
			end

			if toRescueIDX > #toRescue then
				assigned[atlases[a]] = nil
			else
				if Spring.ValidUnitID(atlases[a]) then
					if assigned[atlases[a]] == nil then 
						assigned[atlases[a]] = toRescue[toRescueIDX]
						toRescueIDX = toRescueIDX + 1
					else
						for r = 1, #rescued, 1 do
							if assigned[atlases[a]] == rescued[r] then
								assigned[atlases[a]] = toRescue[toRescueIDX]
								toRescueIDX = toRescueIDX + 1
								break
							end
						end
					end
				end
			end
		end
	end

	return assigned
end