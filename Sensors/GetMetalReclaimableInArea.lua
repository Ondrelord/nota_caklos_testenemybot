local sensorInfo = {
	name = "GetMetalReclaimableInArea",
	desc = "Get list of features with reclaimable metal in certain area.",
	author = "Ondrelord",
	date = "2018-06-11",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(position, radius)

	local features = Spring.GetFeaturesInCylinder(position.x, position.z, radius)
	local reclaimable = {}

	for i = 1, #features, 1 do
		local featID = Spring.GetFeatureDefID(features[i])
		if FeatureDefs[featID].reclaimable and FeatureDefs[featID].metal > 0 then
			reclaimable[#reclaimable + 1] = features[i]
		end
	end
	return reclaimable
end
