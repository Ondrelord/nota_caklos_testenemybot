local sensorInfo = {
	name = "reinforceSquad",
	desc = "Reinforces squad with new units, to a max number of count.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(squad, reinforcement, count)
	local newSquad = {}

	for _,v in pairs(squad) do
		newSquad[#newSquad + 1] = v
	end

	for i = 1, #reinforcement and count, 1 do
		newSquad[#newSquad + 1] = reinforcement[i]
	end

	return newSquad
end