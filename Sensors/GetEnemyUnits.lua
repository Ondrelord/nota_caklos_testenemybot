local sensorInfo = {
	name = "GetFriendlyUnits",
	desc = "Get all firendly units in area.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(center, radius)
	if center.x == nil or center.z == nil then
		return {}
	end

	local team =  Spring.GetLocalTeamID()
	local all_units = Spring.GetUnitsInCylinder(center.x, center.z, radius)
	local enemy_units = {}

	for i = 1, #all_units, 1 do
		if Spring.GetUnitTeam(all_units[i]) ~= team then
			enemy_units[#enemy_units + 1] = all_units[i]
		end
	end
	return enemy_units
end