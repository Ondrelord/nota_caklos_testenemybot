local sensorInfo = {
	name = "testsens",
	desc = "test",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(assigned, toAssign)

	for i = 1, #toAssign, 1 do
		local found = false
		for a = 1, #assigned, 1 do
			if toAssign[i] == assigned[a] then
				found = true
				break
			end
		end

		if not found then
			assigned[#assigned + 1] = toAssign[i]
		end
	end

	return assigned
end