local sensorInfo = {
	name = "GetEnemiesInLOS",
	desc = "Gets list of enemies in line of sight of a unit.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(unit)
	local defID = Spring.GetUnitDefID(unit)
	local x,y,z = Spring.GetUnitPosition(unit)
	local enemies = Sensors.nota_caklos_testenemybot.GetEnemyUnits(Vec3(x,y,z), UnitDefs[defID].losRadius)
	
	return enemies
end