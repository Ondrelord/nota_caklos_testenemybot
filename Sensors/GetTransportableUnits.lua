local sensorInfo = {
	name = "GetTransportableUnits",
	desc = "Get all firendly transportable units.",
	author = "Ondrelord",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(center, radius)
	local team =  Spring.GetLocalTeamID()
	local my_units = Spring.GetTeamUnits(team)
	local transp_units = {}

	for i = 1, #my_units, 1 do
		local x,y,z = Spring.GetUnitPosition(my_units[i])
		local unitDefID = Spring.GetUnitDefID(my_units[i])

		if not (UnitDefs[unitDefID].isBuilding or UnitDefs[unitDefID].isAirUnit) then
			if math.sqrt((x - center.x) * (x - center.x) + (z - center.z) * (z - center.z)) >= radius then
				transp_units[#transp_units + 1] = my_units[i]
			end
		end
	end

	return transp_units
end